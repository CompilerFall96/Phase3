grammar AtalkPass1;

program:
        { int actorCount = 0; }
        { Utils.print("Pass1 started -------------------------"); }
        { Utils.beginScope(); }
		(actor {actorCount++;} | NL)*
		{ Utils.endScope(); }
		{ Utils.checkActorCount(actorCount); }
		{ if (Utils.errorFree) Utils.printErrorFreeLogs(); }
		{ Utils.print("Pass1 finished -------------------------"); }
	;

actor:
		'actor' actor_name = ID '<' actor_queue_size = CONST_NUM '>' NL
		{ Utils.addActor($actor_name.text, $actor_queue_size.int, $actor_name.getLine()); }
            { Utils.beginScope(); }
            { Utils.setActorSymbolTable($actor_name.text); }
			(state | receiver | NL)*
    		{ Utils.endScope(); }
		'end' (NL | EOF)

	;

state:
		type_name = type var_name = ID
		{ Utils.addGlobalVar($var_name.text, $type_name.return_type, $var_name.getLine()); }
		(',' var_name = ID
            { Utils.addGlobalVar($var_name.text, $type_name.return_type, $var_name.getLine()); }
		)* NL
	;

receiver:
        { ArrayList<Variable> vars = new ArrayList<>(); }
        { ArrayList<Type> args = new ArrayList<>(); }
		'receiver' receiver_name = ID '('
		    (type_name = type var_name = ID
                { vars.add(new Variable($var_name.text, $type_name.return_type)); }
                { args.add($type_name.return_type); }
		    (',' type_name = type var_name = ID
                { vars.add(new Variable($var_name.text, $type_name.return_type)); }
                { args.add($type_name.return_type); }
		    )*)? ')' NL
		    { Utils.addReceiver($receiver_name.text, args, $receiver_name.getLine()); }
		    { Utils.beginScope(); }
		    { Utils.addReceiverArgs(vars, $receiver_name.getLine()); }
			statements[false]
		    { Utils.endScope(); }
		'end' NL
	;

type returns [Type return_type]:
		('char' { $return_type = CharType.getInstance(); } | 'int' { $return_type = IntType.getInstance(); } )
	    { ArrayList<Integer> sizes = new ArrayList<>(); }
	    ('[' size = CONST_NUM ']' { sizes.add($size.int); } )*
	    {
	        for (int i = sizes.size()-1; i >= 0; i--) {
                if (sizes.get(i) <= 0) {
                    Utils.print(String.format("[Line #%s] Array with invalid size", $size.getLine()));
                    $return_type = new ArrayType($return_type, 0);
                } else {
                    $return_type = new ArrayType($return_type, sizes.get(i));
                }
	        }
	    }
	;

block [boolean has_for_each]:
		'begin' NL
		    { Utils.beginScope(); }
			statements[$has_for_each]
		    { Utils.endScope(); }
		'end' NL
	;

statements [boolean has_for_each]:
		(s = statement[$has_for_each] | NL)*
	;

statement [boolean has_for_each]:
		stm_vardef
	|	stm_assignment
	|	stm_foreach[true]
	|	stm_if_elseif_else[$has_for_each]
	|	stm_quit
	|	stm_break[$has_for_each]
	|	stm_tell
	|	stm_write
	|	block[$has_for_each]
	;

stm_vardef:
		type_name = type var_name = ID
	    { Utils.addLocalVar($var_name.text, $type_name.return_type, $var_name.getLine()); } ('=' expr)?
		(',' var_name = ID
		    { Utils.addLocalVar($var_name.text, $type_name.return_type, $var_name.getLine()); } ('=' expr)?
        )* NL
	;

stm_tell:
		(ID | 'sender' | 'self') '<<' ID '(' (expr (',' expr)*)? ')' NL
	;

stm_write:
		'write' '(' expr ')' NL
	;

stm_if_elseif_else [boolean has_for_each]:
		'if' expr NL
		    { Utils.beginScope(); }
			statements[$has_for_each]
		    { Utils.endScope(); }
		('elseif' expr NL
            { Utils.beginScope(); }
			statements[$has_for_each]
            { Utils.endScope(); })*
		('else' NL
            { Utils.beginScope(); }
			statements[$has_for_each]
            { Utils.endScope(); })?
		'end' NL
	;

stm_foreach [boolean has_for_each]:
		'foreach' var_name = ID 'in' expr NL
            { Utils.beginScope(); }
            { Utils.addTempVar($var_name.text, NoType.getInstance(), $var_name.getLine()); }
            statements[$has_for_each]
            { Utils.endScope(); }
		'end' NL
	;

stm_quit:
		'quit' NL
	;

stm_break [boolean has_for_each]:
		brk = 'break' NL
		{ Utils.checkBreakScope($has_for_each, $brk.getLine()); }
	;

stm_assignment:
		expr NL
	;

expr:
		expr_assign
	;

expr_assign:
		expr_or '=' expr_assign
	|	expr_or
	;

expr_or:
		expr_and expr_or_tmp
	;

expr_or_tmp:
		'or' expr_and expr_or_tmp
	|
	;

expr_and:
		expr_eq expr_and_tmp
	;

expr_and_tmp:
		'and' expr_eq expr_and_tmp
	|
	;

expr_eq:
		expr_cmp expr_eq_tmp
	;

expr_eq_tmp:
		('==' | '<>') expr_cmp expr_eq_tmp
	|
	;

expr_cmp:
		expr_add expr_cmp_tmp
	;

expr_cmp_tmp:
		('<' | '>') expr_add expr_cmp_tmp
	|
	;

expr_add:
		expr_mult expr_add_tmp
	;

expr_add_tmp:
		('+' | '-') expr_mult expr_add_tmp
	|
	;

expr_mult:
		expr_un expr_mult_tmp
	;

expr_mult_tmp:
		('*' | '/') expr_un expr_mult_tmp
	|
	;

expr_un:
		('not' | '-') expr_un
	|	expr_mem
	;

expr_mem:
		expr_other expr_mem_tmp
	;

expr_mem_tmp:
		'[' expr ']' expr_mem_tmp
	|
	;

expr_other:
		CONST_NUM
	|	CONST_CHAR
	|	CONST_STR
	|	ID
	|	'{' expr (',' expr)* '}'
	|	'read' '(' CONST_NUM ')'
	|	'(' expr ')'
	;

CONST_NUM:
		('-')?[0-9]+
	;

CONST_CHAR:
		'\'' . '\''
	;

CONST_STR:
		'"' ~('\r' | '\n' | '"')* '"'
	;

NL:
		'\r'? '\n' { setText("new_line"); }
	;

ID:
		[a-zA-Z_][a-zA-Z0-9_]*
	;

COMMENT:
		'#'(~[\r\n])* -> skip
	;

WS:
    	[ \t] -> skip
    ;