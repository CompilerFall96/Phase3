grammar AtalkPass2;

program:
        {
            Utils.print("Pass2 started -------------------------");
            Pass2Utils.beginScope();
        }
		(actor| NL)*
		{
            Pass2Utils.endScope();
            Utils.print("Pass2 finished -------------------------");
        }
	;

actor:
		'actor' actor_name = ID '<' actor_queue_size = CONST_NUM '>' NL
            { Pass2Utils.beginScope(); }
			(state | receiver | NL)*
            { Pass2Utils.endScope(); }
		'end' (NL | EOF)

	;

state:
		type_name = type var_name = ID (',' var_name = ID)* NL
	;

receiver:
        { int counter = 0; }
		'receiver' receiver_name = ID '('
		    (type ID {counter++;}
		    (',' type ID {counter++;})*)? ')' NL
		    { Pass2Utils.beginScope(); }
		    { SymbolTable.define(counter); }
			statements[$receiver_name.text.equals("init") && (counter == 0)]
		    { Pass2Utils.endScope(); }
		'end' NL
	;

type returns [Type return_type]:
		('char' { $return_type = CharType.getInstance(); } | 'int' { $return_type = IntType.getInstance(); } )
        { ArrayList<Integer> sizes = new ArrayList<>(); }
        ('[' size = CONST_NUM ']' { sizes.add($size.int); } )*
        {
            for (int i = sizes.size()-1; i >= 0; i--) {
                if (sizes.get(i) <= 0) {
                    Utils.print(String.format("[Line #%s] Array with invalid size", $size.getLine()));
                    $return_type = new ArrayType($return_type, 0);
                } else {
                    $return_type = new ArrayType($return_type, sizes.get(i));
                }
            }
        }
	;

block[boolean isInInitReceiver]:
		'begin' NL
		    { Pass2Utils.beginScope(); }
			statements[$isInInitReceiver]
		    { Pass2Utils.endScope(); }
		'end' NL
	;

statements[boolean isInInitReceiver]:
		(statement[$isInInitReceiver] | NL)*
	;

statement[boolean isInInitReceiver]:
		stm_vardef
	|	stm_assignment
	|	stm_foreach[$isInInitReceiver]
	|	stm_if_elseif_else[$isInInitReceiver]
	|	stm_quit
	|	stm_break
	|	stm_tell[$isInInitReceiver]
	|	stm_write
	|	block[$isInInitReceiver]
	;

stm_vardef:
		type var_name = ID
	    { SymbolTable.define(); }
	    ('=' expr {
	        Pass2Utils.checkTypesEquality($type.return_type, $expr.return_type, $var_name.line);
	    })?
		(',' var_name = ID
		    { SymbolTable.define(); }
		    ('=' expr
		    {
                Pass2Utils.checkTypesEquality($type.return_type, $expr.return_type, $var_name.line);
		    }
		    )?
        )* NL
	;

stm_tell[boolean isInInitReceiver]:
        { ArrayList<Type> types = new ArrayList<>(); boolean actorExists = true;}
		(actorName = ID
		{
            SymbolTableItem item = SymbolTable.top.get($actorName.text);
            if(item == null || !(item instanceof SymbolTableActorItem)) {
                actorExists = false;
                Utils.printError("Actor " + $actorName.text + " doesn't exist.", $actorName.line );
            }
		}
		| actorName  = 'sender' | actorName = 'self') '<<'
		 receiverName = ID '(' (expr { types.add($expr.return_type); } (',' expr { types.add($expr.return_type); })*)? ')'
		{
            Receiver receiver = new Receiver($receiverName.text, types);
            SymbolTableItem item;
            switch($actorName.text) {
                case "sender":
                //emtiazi
                    if ($isInInitReceiver)
                        Utils.printError("Illegal use of 'sender' in init without arguments receiver", $actorName.line);
                break;
                case "self":
                    item = SymbolTable.top.get(receiver.getName());
                    if(item == null || !(item instanceof SymbolTableReceiverItem)) {
                        Utils.printError("Receiver " + $receiverName.text + " with given argument not found.", $actorName.line );
                    }
                    else {
                        SymbolTableReceiverItem var = (SymbolTableReceiverItem) item;
                        Pass2Utils.print("Receiver " + $receiverName.text + " called.", $receiverName.line);
                    }
                break;
                default:
                    if(actorExists) {
                        SymbolTableActorItem actorItem = (SymbolTableActorItem) SymbolTable.top.get($actorName.text);
                        item = actorItem.getActorSymbolTable().get(receiver.getName());
                        if(item == null || !(item instanceof SymbolTableReceiverItem)) {
                           Utils.printError("Receiver " + $receiverName.text + " with given argument not found in actor " + $actorName.text, $actorName.line );
                        }
                        else {
                           SymbolTableReceiverItem var = (SymbolTableReceiverItem) item;
                           Pass2Utils.print("Receiver " + $receiverName.text + " from actor " + $actorName.text + " called.", $receiverName.line);
                        }
                    }
                break;
            }
		}
		NL
	;

stm_write:
		id='write' '(' expr
		 {
		    if(!Pass2Utils.isWriteArgument($expr.return_type))
                Utils.printError("Invalid write argument: " + $expr.return_type.toString() , $id.line);
            else
                Pass2Utils.print("Write function called.", $id.line);
		 }
		 ')' NL
	;

stm_if_elseif_else[boolean isInInitReceiver]:
		'if' expr NL
		    { Pass2Utils.beginScope(); }
			statements[$isInInitReceiver]
		    { Pass2Utils.endScope(); }
		('elseif' expr NL
            { Pass2Utils.beginScope(); }
			statements[$isInInitReceiver]
            { Pass2Utils.endScope(); })*
		('else' NL
            { Pass2Utils.beginScope(); }
			statements[$isInInitReceiver]
            { Pass2Utils.endScope(); })?
		'end' NL
	;

stm_foreach[boolean isInInitReceiver]:
		'foreach' id=ID 'in' expr NL
            { Pass2Utils.beginScope(); }
            {
                if (!($expr.return_type instanceof ArrayType)){
                    Utils.printError("Can't iterate in foreach expression", $id.line);
                    Pass2Utils.updateForeachVar($id.text, NoType.getInstance(), $id.line);
                }
                else
                    Pass2Utils.updateForeachVar($id.text, ((ArrayType) $expr.return_type).getType(), $id.line);

            }
            statements[$isInInitReceiver]
            { Pass2Utils.endScope(); }
		'end' NL
	;

stm_quit:
		'quit' NL
	;

stm_break:
		brk = 'break' NL
	;

stm_assignment:
		expr NL
	;

expr returns [Type return_type]:
		expr_assign { $return_type = $expr_assign.return_type; }
	;

expr_assign returns [Type return_type]:
		expr_or id='=' expr_assign
        {
            $return_type = Pass2Utils.getAssignReturnType($expr_or.return_type, $expr_assign.return_type, $id.line);
            if (!$expr_or.isLValue)
                Utils.printError("Invalid left value in assignment", $id.line);
        }
	|	expr_or { $return_type = $expr_or.return_type; }
	;

expr_or returns [Type return_type, boolean isLValue]:
		expr_and expr_or_tmp
		{
            if ($expr_or_tmp.return_type == null) {
                $return_type = $expr_and.return_type;
                $isLValue = $expr_and.isLValue;
            }
            else {
                $return_type = Pass2Utils.getOperatorReturnType($expr_and.return_type, $expr_or_tmp.line, "or");
                $isLValue = false;
            }
        }
	;

expr_or_tmp returns [Type return_type, int line]:
		id='or' expr_and expr_or_tmp
        {
            $line = $id.line;
            $return_type = Pass2Utils.getOperatorReturnType($expr_and.return_type, $id.line, $id.text);
        }
    |   { $return_type = null; }
	;

expr_and returns [Type return_type, boolean isLValue]:
		expr_eq expr_and_tmp
		{
            if ($expr_and_tmp.return_type == null) {
                $return_type = $expr_eq.return_type;
                $isLValue = $expr_eq.isLValue;
            }
            else {
                $return_type = Pass2Utils.getOperatorReturnType($expr_eq.return_type, $expr_and_tmp.line, "and");
                $isLValue = false;
            }
        }
	;

expr_and_tmp returns [Type return_type, int line]:
		id='and' expr_eq expr_and_tmp
        {
            $line = $id.line;
            $return_type = Pass2Utils.getOperatorReturnType($expr_eq.return_type, $id.line, $id.text);
        }
    |   { $return_type = null; }
	;

expr_eq returns [Type return_type, boolean isLValue]:
		expr_cmp expr_eq_tmp
		{
		    if ($expr_eq_tmp.return_type == null) {
		        $return_type = $expr_cmp.return_type;
		        $isLValue = $expr_cmp.isLValue;
            }
            else {
                $return_type = Pass2Utils.getCompareReturnType($expr_cmp.return_type, $expr_eq_tmp.return_type, $expr_eq_tmp.line);
                $isLValue = false;
            }
		}
	;

expr_eq_tmp returns [Type return_type, int line]:
		id=('==' | '<>') expr_cmp expr_eq_tmp {$return_type = $expr_cmp.return_type; $line = $id.line;}
	| {$return_type = null;}
	;

expr_cmp returns [Type return_type, boolean isLValue]:
		expr_add expr_cmp_tmp
		{
            if ($expr_cmp_tmp.return_type == null) {
                $return_type = $expr_add.return_type;
                $isLValue = $expr_add.isLValue;
            }
            else {
                $return_type = Pass2Utils.getOperatorReturnType($expr_add.return_type, $expr_cmp_tmp.line, "");
                $isLValue = false;
            }
        }
	;

expr_cmp_tmp returns [Type return_type, int line]:
		id=('<' | '>') expr_add expr_cmp_tmp
        {
            $line = $id.line;
            $return_type = Pass2Utils.getOperatorReturnType($expr_add.return_type, $id.line, $id.text);
        }
    |   { $return_type = null; }
	;

expr_add returns [Type return_type, boolean isLValue]:
		expr_mult expr_add_tmp
		{
            if ($expr_add_tmp.return_type == null) {
                $return_type = $expr_mult.return_type;
                $isLValue = $expr_mult.isLValue;
            }
            else {
                $return_type = Pass2Utils.getOperatorReturnType($expr_mult.return_type, $expr_add_tmp.line, "");
                $isLValue = false;
            }
        }
	;

expr_add_tmp returns [Type return_type, int line]:
		id=('+' | '-') expr_mult expr_add_tmp
        {
            $line = $id.line;
            $return_type = Pass2Utils.getOperatorReturnType($expr_mult.return_type, $id.line, $id.text);
        }
    |   { $return_type = null; }
	;

expr_mult returns [Type return_type, boolean isLValue]:
		expr_un expr_mult_tmp
		{
		    if ($expr_mult_tmp.return_type == null) {
		        $return_type = $expr_un.return_type;
                $isLValue = $expr_un.isLValue;
            }
		    else {
		        $return_type = Pass2Utils.getOperatorReturnType($expr_un.return_type, $expr_mult_tmp.line, "");
                $isLValue = false;
            }
		}
	;

expr_mult_tmp returns [Type return_type, int line]:
		id=('*' | '/') expr_un expr_mult_tmp
		{
		    $line = $id.line;
            $return_type = Pass2Utils.getOperatorReturnType($expr_un.return_type, $id.line, $id.text);
		}
	|   { $return_type = null; }
	;

expr_un returns [Type return_type, boolean isLValue]:
		id=('not' | '-') expr_un
		{
            $return_type = Pass2Utils.getOperatorReturnType($expr_un.return_type, $id.line, $id.text);
            $isLValue = false;
		}
	|	expr_mem { $return_type = $expr_mem.return_type; $isLValue = $expr_mem.isLValue;}
	;

expr_mem returns [Type return_type, boolean isLValue]:
		expr_other expr_mem_tmp
		{
		    $isLValue = $expr_other.isLValue;
		    if($expr_mem_tmp.count == 0)
		        $return_type = $expr_other.return_type;
		    else if(!($expr_other.return_type instanceof ArrayType)) {
                Utils.printError("Invalid Use of array.", $expr_other.line);
                $return_type = NoType.getInstance();
            }
            else {
                if ($expr_other.return_type.getDimension() < $expr_mem_tmp.count){
                    Utils.printError("Invalid use of array.", $expr_other.line);
                    $return_type = NoType.getInstance();
                }
                else {
                    $return_type = ((ArrayType) $expr_other.return_type).getChildType($expr_mem_tmp.count);
                }
            }
		}
	;

expr_mem_tmp returns [int count]:
		id='[' expr ']' expression = expr_mem_tmp {$count = $expression.count + 1;}
	| {$count = 0;}
	;

expr_other returns [Type return_type, int line, boolean isLValue]:
		num = CONST_NUM { $return_type = IntType.getInstance(); $line = $num.line; $isLValue = false; }
	|	ch = CONST_CHAR { $return_type = CharType.getInstance(); $line = $ch.line; $isLValue = false; }
	|	str = CONST_STR { $return_type = new ArrayType(CharType.getInstance(), $str.text.length()-2); $line = $str.line; $isLValue = false;}
	|	id = ID
	{
	    $line = $id.line;
	    $isLValue = true;
        SymbolTableItem item = SymbolTable.top.get($id.text);
        if (item instanceof SymbolTableForeachLocalVariableItem)
            $isLValue = false;
        if(item == null) {
            Utils.printError("Item " + $id.text + " doesn't exist.", $id.getLine());
            Utils.addTempVar($id.text, NoType.getInstance(), $id.line);
            SymbolTable.define();
        }
        else {
            SymbolTableVariableItemBase var = (SymbolTableVariableItemBase) item;
            Pass2Utils.print("Variable " + $id.text + " used.\t\t" + "Base Reg: " + var.getBaseRegister() + ", Offset: " + var.getOffset(), $id.getLine());
        }
        $return_type = ((SymbolTableVariableItemBase) SymbolTable.top.get($id.text)).getVariable().getType();
    }
	|	{ int counter = 1; boolean errorFree = true; $isLValue = false; }
	    id = '{' {$line = $id.line;} expression = expr (',' expression2 = expr
	    {
	        $line = $id.line;
	        if(!$expression2.return_type.equals($expression.return_type) && errorFree) {
	            Utils.printError("Unequal types in array.", $id.line);
	            errorFree = false;
	        }
	        counter++;
	    }
	    )* '}'
	    { $return_type = new ArrayType($expression.return_type, counter); }
	|	'read' '(' str = CONST_NUM ')' { $return_type = new ArrayType(CharType.getInstance(), $str.int); $line = $str.line; $isLValue = false;}
	|	id = '(' expression = expr ')' { $return_type = $expression.return_type; $line = $id.line; $isLValue = false;}
	;

CONST_NUM:
		('-')?[0-9]+
	;

CONST_CHAR:
		'\'' . '\''
	;

CONST_STR:
		'"' ~('\r' | '\n' | '"')* '"'
	;

NL:
		'\r'? '\n' { setText("new_line"); }
	;

ID:
		[a-zA-Z_][a-zA-Z0-9_]*
	;

COMMENT:
		'#'(~[\r\n])* -> skip
	;

WS:
    	[ \t] -> skip
    ;