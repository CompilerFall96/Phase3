import java.util.ArrayList;

public class Receiver {
    public Receiver(String name, ArrayList<Type> args) {
        this.name = name;
        this.args = args;
    }

    public String getName() {
        String postfix = "";
        for(Type type: args) {
            postfix += type.toString() + "_";
        }
        String result = name + "_" + postfix;
        return result.substring(0, result.length() - 1);
    }

    public ArrayList<Type> getArgs() {
        return args;
    }

    @Override
    public String toString() {
        return String.format("Receiver %s with args:  %s", name, Utils.printArray(args));
    }

    private String name;
    private ArrayList<Type> args;
}
