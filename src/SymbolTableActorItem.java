public class SymbolTableActorItem extends SymbolTableItem{
    public SymbolTableActorItem (Actor actor) {
        this.actor = actor;
    }

    @Override
    public String getKey() {
        return this.actor.getName();
    }

    @Override
    public boolean useMustBeComesAfterDef() {
        return false;
    }

    private Actor actor;
    private SymbolTable actorSymbolTable;

    public SymbolTable getActorSymbolTable() {
        return actorSymbolTable;
    }

    public void setActorSymbolTable(SymbolTable actorSymbolTable) {
        this.actorSymbolTable = actorSymbolTable;
    }
}
