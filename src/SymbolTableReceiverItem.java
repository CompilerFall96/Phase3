public class SymbolTableReceiverItem extends SymbolTableItem{
    public SymbolTableReceiverItem(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public String getKey() {
        return this.receiver.getName();
    }

    @Override
    public boolean useMustBeComesAfterDef() {
        return false;
    }

    public Receiver getReceiver() {
        return receiver;
    }

    private Receiver receiver;
}
