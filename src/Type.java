public abstract class Type {

	public abstract int size();

	public abstract boolean equals(Object other);

	public abstract String toString();

	public int getDimension() {
		return 0;
	}

	public static final int WORD_BYTES = 4;
	public static final int CHAR_BYTE = 1;
	public static final int NO_BYTE = 0;

	public String getIdentifier() {
		return this.toString();
	}

	public Type getBaseType() {
		return this;
	}
}