import java.util.ArrayList;

public class Utils {
    public static ArrayList<String> errorFreeLogs = new ArrayList<>();
    public static boolean errorFree = true;
    private static int scope = 0;
    public static void print(String str){
        System.out.println(str);
    }

    public static void printErrorFreeLogs() {
        for (String log : errorFreeLogs)
            print(log);
    }

    public static void addLocalVar(String name, Type type, int line) {
        try {
            putLocalVar(name, type);
        } catch (ItemAlreadyExistsException e) {
            print(String.format("[Line #%s] Local Variable \"%s\" already exists.", line, name));
            errorFree = false;
            int count = 1;
            while(true) {
                try {
                    putLocalVar(name + "_Temproray_" + count, type);
                    break;
                } catch (ItemAlreadyExistsException e2) {
                    count++;
                }
            }
        }
    }

    public static void putLocalVar(String name, Type type) throws ItemAlreadyExistsException {
        SymbolTable.top.put(
                new SymbolTableLocalVariableItem(
                        new Variable(name, type),
                        SymbolTable.top.getOffset(Register.SP)
                )
        );
    }

    public static void addActor(String name, int queueLength, int line){
        try {
            if (queueLength <= 0) {
                Utils.print(String.format("[Line #%s] Actor \"%s\" with invalid queue length.", line, name));
                errorFree = false;
                queueLength = 0;
            }
            Utils.putActor(name, queueLength);
            errorFreeLogs.add("Actor " + name + " with queueLength " + queueLength + " defined!");
        } catch (ItemAlreadyExistsException e) {
            Utils.print(String.format("[Line #%s] Actor \"%s\" already exists.", line, name));
            errorFree = false;
            int count = 1;
            while(true) {
                try {
                    Utils.putActor(name + "_Temproray_" + count, queueLength);
                    break;
                } catch (ItemAlreadyExistsException e2) {
                    count++;
                }
            }
        }
    }

    public static void putActor(String name, int size) throws ItemAlreadyExistsException {
        SymbolTable.top.put(
                new SymbolTableActorItem(
                        new Actor(name, size)
                )
        );
    }

    public static void putGlobalVar(String name, Type type) throws ItemAlreadyExistsException {
        SymbolTable.top.put(
                new SymbolTableGlobalVariableItem(
                        new Variable(name, type),
                        SymbolTable.top.getOffset(Register.GP)
                )
        );
    }

    public static void addGlobalVar(String name, Type type, int line) {
        try {
            putGlobalVar(name, type);
        } catch (ItemAlreadyExistsException e) {
            print(String.format("[Line #%s] Global Variable \"%s\" already exists.", line, name));
            errorFree = false;
            int count = 1;
            while(true) {
                try {
                    putGlobalVar(name + "_Temproray_" + count, type);
                    break;
                } catch (ItemAlreadyExistsException e2) {
                    count++;
                }
            }
        }
    }

    public static void putTempVar(String name, Type type) throws ItemAlreadyExistsException {
        SymbolTable.top.put(
            new SymbolTableLocalTempVariableItem(
                new Variable(name, type),
                SymbolTable.top.getOffset(Register.GP)
            )
        );
    }

    public static void addTempVar(String name, Type type, int line) {
        try {
            putTempVar(name, type);
        } catch (ItemAlreadyExistsException e) {
            int count = 1;
            while(true) {
                try {
                    putTempVar(name + "_Temproray_" + count, type);
                    break;
                } catch (ItemAlreadyExistsException e2) {
                    count++;
                }
            }
        }
    }

    public static String printArray(ArrayList<Type> list) {
        String result = "";
        StringBuilder stringBuilder = new StringBuilder(result);
        for(Type obj: list) {
            stringBuilder.append(obj.toString());
            stringBuilder.append(", ");
        }
        return stringBuilder.toString();
    }

    public static void putReceiver(String name, ArrayList<Type> args) throws ItemAlreadyExistsException {
        SymbolTable.top.put(
                new SymbolTableReceiverItem(new Receiver(name, args))
        );
    }

    public static void addReceiver(String name, ArrayList<Type> args, int line) {
        try {
            putReceiver(name, args);
        } catch (ItemAlreadyExistsException e) {
            print(String.format("[Line #%s] Global Variable \"%s\" already exists.", line, name));
            errorFree = false;
            int count = 1;
            while (true) {
                try {
                    putReceiver(name + "_Temproray_" + count, args);
                    break;
                } catch (ItemAlreadyExistsException e2) {
                    count++;
                }
            }
        }
    }

    public static void addReceiverArgs(ArrayList<Variable> args, int line) {
        for (Variable v : args)
            addLocalVar(v.getName(), v.getType(), line);
    }

    public static void beginScope() {
//        print("+In Scope: " + scope++);
        int offset = 0;
        int globalOffset = 0;
        if(SymbolTable.top != null) {
            offset = SymbolTable.top.getOffset(Register.SP);
            globalOffset = SymbolTable.top.getOffset(Register.GP);
        }

        SymbolTable.push(new SymbolTable(SymbolTable.top));
        SymbolTable.top.setOffset(Register.SP, offset);
        SymbolTable.top.setOffset(Register.GP, globalOffset);
    }

    public static void endScope() {
        errorFreeLogs.add("Stack offset: " + SymbolTable.top.getOffset(Register.SP) + " Global offset: " + SymbolTable.top.getOffset(Register.GP));

        if(SymbolTable.top.getPreSymbolTable() != null) {
            SymbolTable.top.getPreSymbolTable().setOffset(
                    Register.GP,
                    SymbolTable.top.getOffset(Register.GP)
            );
        }

        SymbolTable.pop();
        scope--;
//        print("-Out Scope: " + scope);
    }

    public static void checkActorCount(int actorCount){
        if (actorCount == 0) {
            Utils.print("No actor has been defined in program");
            errorFree = false;
        }
    }

    public static void checkBreakScope(boolean has_for_each, int line) {
        if (!has_for_each) {
            Utils.print(String.format("[Line #%s] Break not in foreach.", line));
            errorFree = false;
        }
    }

    public static void printError(String message, int line) {
        Utils.print(String.format("[Line #%s] %s", line, message));
    }

    public static void setActorSymbolTable(String actor_name) {
        SymbolTableActorItem var = (SymbolTableActorItem) SymbolTable.top.get(actor_name);
        var.setActorSymbolTable(SymbolTable.top);
    }
}
